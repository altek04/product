angular.module 'product'
.controller 'MainController', ($timeout, webDevTec, toastr,$http) ->
  'ngInject'
  vm = this
  vm.flaglist = true
  vm.inlist = true
  vm.saved = localStorage.getItem('games')
  vm.local_list = if localStorage.getItem('games') != null then JSON.parse(vm.saved) else []
  localStorage.setItem 'games', JSON.stringify(vm.local_list)

  getProducts = ->
    $http.get('assets/data/game.json')
      .then (response) ->
        vm.products = response.data
        for product in vm.products
          for item in vm.local_list
            if item.id == product.id
              product.flag = false
    return

  addLocalStorage = (item) ->
    if !(item in vm.local_list)
      vm.local_list.push item
      localStorage.setItem 'games', JSON.stringify(vm.local_list)
      toastr.success 'Add product in cart'
      getProducts()
    return


  vm.addLocalStorage = addLocalStorage
  vm.getProducts = getProducts

  getProducts()
  return
