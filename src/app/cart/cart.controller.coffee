angular.module 'product'
.controller 'CartController', ($timeout, webDevTec, toastr) ->
  'ngInject'
  vm = this
  vm.summprice = 0
  vm.saved = localStorage.getItem('games')
  vm.products = if localStorage.getItem('games') != null then JSON.parse(vm.saved) else []
  localStorage.setItem 'games', JSON.stringify(vm.products)

  sumPrice = ->
    if vm.products.length > 0
      i = 0
      while i < vm.products.length
        game = vm.products[i]
        vm.summprice += game.price
        i++
    return

  deleteLocalStorage = (item, index) ->
    i = 0
    while i < vm.products.length
      game = vm.products[i]
      if i == index
        vm.products.splice i, 1
        localStorage.setItem 'games', JSON.stringify(vm.products)
      i++
    return

  vm.deleteLocalStorage = deleteLocalStorage
  vm.sumPrice = sumPrice

  sumPrice()
  return
